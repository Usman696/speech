filepath = "/home/usman/code/project/site1/files/audio/"
bucketname = "first-speech-bucket"

from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post
from .forms import PostForm
#from django.views.generic import ListView, CreateView 
#from django.urls import reverse_lazy 
#from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
#from django.core.files.storage import FileSystemStorage

#import speech_recognition as sr
import librosa
import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt
import os
#import glob
#from tqdm import tqdm_notebook
import warnings
warnings.simplefilter('ignore')
#import IPython
#from os import path
from pydub import AudioSegment
#from pyparsing import Word, OneOrMore, alphanums
import re
from collections import Counter
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import wave
from google.cloud import storage

'''
def post_list(request):
	posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
	return render(request, 'recognitionpost/post_list.html', {'posts': posts})
'''
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'recognitionpost/post_detail.html', {'post': post})

def name_parsing(src):
    if ' ' in src:
        src = re.sub(r'[ ]', '_', src)
    if '"' in src:
        src = re.sub(r'["]', '22', src)
    signs = '~`!@#$%^&*()+=<>,\'№'
    for sign in signs:
        if sign in src:
            src = re.sub(r'[~`!@#$%^&*()+=<>,\'№]', '', src)
    audio_name_parsing = src.split('.')
    audio_format = audio_name_parsing[-1]
    #print('Парсинг', audio_name_parsing)
    name = ''
    for i in audio_name_parsing[:-1]:
        name += i + '.'
    return(src, audio_format, name)

def format(src, audio_format, name):
    sound = AudioSegment.from_file(filepath + src, audio_format)
    sound.export(filepath + name + ".wav", format="wav")

def stereo_to_mono(audio_file_name):
    sound = AudioSegment.from_wav(audio_file_name)
    sound = sound.set_channels(1)
    sound.export(audio_file_name, format="wav")


def frame_rate_channel(audio_file_name):
    with wave.open(audio_file_name, "rb") as wave_file:
        frame_rate = wave_file.getframerate()
        channels = wave_file.getnchannels()
        return frame_rate,channels


def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)


def delete_blob(bucket_name, blob_name):
    """Deletes a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)

    blob.delete()

def count_badparasites(text, words_timestamps):
    with open("bad.txt", 'r') as bad_words_txt:
        bad_words_dict = {}
        i = 0
        for line in bad_words_txt:
            for word in re.sub(r'[ \n|\ufeff]', '', line).split(','):
                bad_words_dict[word] = i
                i += 1
        #print(bad_words_dict)


    with open("parasit.txt", 'r') as parasite_words_txt:
        parasite_two_words = []
        parasite_one_words_dict = {}
        i = 0
        for line in parasite_words_txt:
            line = re.sub(r'[\n]', '', line)
            if len(parasite_two_words) < 27:
                parasite_two_words.append(line)
            else:
                parasite_one_words_dict[line] = i
                i += 1
        #print(parasite_two_words, parasite_one_words_dict)

    #время и счетчик для матов
    bad_words_counter = 0
    parasites_in_text = []
    bad_words_in_text = []


    for i in range(len(words_timestamps)):
        #print(words_timestamps[i][0])
        if bad_words_dict.get(words_timestamps[i][0]):
            bad_words_in_text.append(words_timestamps[i])
            bad_words_counter += 1
        elif '*' in words_timestamps[i][0]:
            bad_words_in_text.append(words_timestamps[i])
            bad_words_counter += 1
        elif parasite_one_words_dict.get(words_timestamps[i][0]):
            parasites_in_text.append(words_timestamps[i])
    #print(bad_words_in_text, parasites_in_text)
    two_words_parasites_in_text = []            
    for parasite_two_word in parasite_two_words:
        if parasite_two_word in text:
            two_words_parasites_in_text.append(parasite_two_word)

    count_parasites_in_text = []
    for i in range(len(parasites_in_text)):
        count_parasites_in_text.append(parasites_in_text[i][0])
    #print(count_parasites_in_text, two_words_parasites_in_text)
    counted_parasites = dict(Counter(count_parasites_in_text+two_words_parasites_in_text))
    kids_bad_words = [[0 for x in range(2)] for y in range(len(bad_words_in_text))]
    for i in range(len(bad_words_in_text)):
        kids_bad_words[i][0] = bad_words_in_text[i][1]
        kids_bad_words[i][1] = bad_words_in_text[i][2]
    return(bad_words_counter, counted_parasites, kids_bad_words, parasites_in_text)


def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            #post.author = request.user
            post.published_date = timezone.now()
            post.save()

            #парсим название аудио и конвертируем в wav
            audio = str(request.FILES['files'])
            audio, audio_format, name = name_parsing(audio)

            if audio_format != "wav":
                format(audio, audio_format, name)
                audio_file_name = name + ".wav"
            else:
                audio_file_name = audio
            #print(audio)

            file_name = filepath + audio_file_name

            frame_rate, channels = frame_rate_channel(file_name)

            if channels > 1:
                stereo_to_mono(file_name)

            bucket_name = bucketname
            source_file_name = filepath + audio_file_name
            destination_blob_name = audio_file_name

            upload_blob(bucket_name, source_file_name, destination_blob_name)
            #print("file uploaded")
            gcs_uri = 'gs://' + bucketname + '/' + audio_file_name
            transcript = ''    

            client = speech.SpeechClient()
            audio = types.RecognitionAudio(uri=gcs_uri)

            config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=frame_rate,
            language_code='ru-RU',
            enable_word_time_offsets=True)

            # Detects speech in the audio file
            operation = client.long_running_recognize(config, audio)
            #print('Waiting for operation to complete...')
            response = operation.result(timeout=1000)
            #print("operation complete")

            words_timestamps_all_results = []
            for result in response.results:
                alternative = result.alternatives[0]
                #transcript += alternative.transcript
                #print(alternative.words, len(alternative.words))
                words_timestamps = [['' for x in range (3) ] for y in range(len(alternative.words))]
                i = 0
                for word_info in alternative.words:
                    word = word_info.word
                    start_time = word_info.start_time
                    end_time = word_info.end_time
                    words_timestamps[i][0] = word
                    words_timestamps[i][1] = start_time.seconds + start_time.nanos * 1e-9
                    words_timestamps[i][2] = end_time.seconds + end_time.nanos * 1e-9
                    i += 1
                for i in range(len(words_timestamps)):
                    words_timestamps_all_results.append(words_timestamps[i])

            delete_blob(bucket_name, destination_blob_name)

            text = ''

            for i in range(len(words_timestamps_all_results)):
                words_timestamps_all_results[i][0] = words_timestamps_all_results[i][0].lower()
                text += words_timestamps_all_results[i][0] + ' '

            bad_words_counter, counted_parasites, bad_words_timestamps, parasites_word_timestamps = count_badparasites(text, words_timestamps_all_results)
            return render(request, 'recognitionpost/post_detail.html', {'text': text, 'bad_words_counter' : bad_words_counter, 'bad_words_timestamps' : bad_words_timestamps, 'parasites_word_timestamps' : parasites_word_timestamps, 'parasites' : counted_parasites, 'post':post})
        #return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
        return render(request, 'recognitionpost/post_edit.html', {'form': form})
